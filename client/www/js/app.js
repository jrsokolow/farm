// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('goatfarm', ['ionic', 'goatfarm.controllers', 'goatfarm.factories', 'goatfarm.values', 'ngMaterial', 'goatfarm.filters'])
  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true)
        cordova.plugins.Keyboard.disableScroll(true)
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault()
      }
    })
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      //   .state('app', {
      //     url: '/app',
      //     abstract: true,
      //     templateUrl: 'templates/menu.html',
      //     controller: 'AppCtrl'
      //   })

      //   .state('login', {
      //     url: '/login',
      //     templateUrl: 'templates/login.html',
      //     controller: 'AppCtrl'
      //   })

      .state('sale', {
        url: '/sale',
        views: {
          'menuContent': {
            templateUrl: 'templates/sale.html',
            controller: 'SaleCtrl'
          }
        }
      })

      .state('sold', {
        url: '/sold',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/sold.html',
            controller: 'SoldCtrl'
            // for blog purpose
            // resolve: {
            //     'sales':function($q, salesFactory) {
            //         var deferred = $q.defer();
            //         salesFactory.getSales().success(function(data) {
            //             deferred.resolve(data);
            //         }).error(function(error) {
            //             deferred.reject(error);
            //         });
            //         return deferred.promise;
            //     }
            // }
          }
        }
      })

      .state('sale_edit', {
        url: '/sale_edit/:saleId',
        views: {
          'menuContent': {
            templateUrl: 'templates/sale.html',
            controller: 'SaleCtrl'
          }
        }
      })

      .state('new_cost', {
        url: '/cost/new',
        views: {
          'menuContent': {
            templateUrl: 'templates/cost.html',
            controller: 'CostCtrl'
          }
        }
      })

      .state('cost_list', {
        url: '/cost/list',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/costs.html',
            controller: 'CostsCtrl'
          }
        }
      })

      .state('cost_edit', {
        url: '/cost/edit/:costId',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/cost.html',
            controller: 'CostCtrl'
          }
        }
      })

      .state('new_milk_stat', {
        url: '/milk_stat/new',
        views: {
          'menuContent': {
            templateUrl: 'templates/milkStat.html',
            controller: 'MilkStatCtrl'
          }
        }
      })

      .state('milk_stat_list', {
        url: '/milk_stat/list',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/milkStats.html',
            controller: 'MilkStatsCtrl'
          }
        }
      })

      .state('milk_stat_edit', {
        url: '/milk_stat/edit/:id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/milkStat.html',
            controller: 'MilkStatCtrl'
          }
        }
      })

      .state('balance', {
        url: '/balance',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/balance.html',
            controller: 'BalanceCtrl'
          }
        }
      })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/sale')
  })
