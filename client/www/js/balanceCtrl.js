/**
 * Created by sokoloj1 on 2017-01-31.
 */

(function() {

  function BalanceCtrl($scope, $filter, balanceFactory) {

    $scope.filter = {
      from:'', to:'', result:''
    };

    balanceFactory.getIncome().then(function (response) {
      $scope.income = response[0].income;
    });

    balanceFactory.getOutcome().then(function (response) {
      $scope.outcome = response[0].outcome;
    });

    $scope.getIncomeInDateRange = function() {
      $scope.filter.from.setHours(0,0,0,0);
      $scope.filter.to.setHours(23,59,59,999);
      var fromDate = $filter('date')($scope.filter.from, "yyyy-MM-dd HH:mm:ss Z");
      var toDate = $filter('date')($scope.filter.to, "yyyy-MM-dd HH:mm:ss Z");
      balanceFactory.getIncomeInDateRange(fromDate, toDate).then(function(response) {
        $scope.filter.result = response[0].income;
      });
    };

  }

  angular.module('goatfarm.controllers').controller('BalanceCtrl', BalanceCtrl);

}());
