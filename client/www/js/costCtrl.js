/**
 * Created by sokoloj1 on 2017-01-31.
 */

(function () {
  function CostCtrl ($scope, $stateParams, $state, $ionicPopup, costFactory) {
    $scope.cost = {}
    $scope.cost.when = new Date()

    $scope.costs = {}

    if ($stateParams.costId) {
      costFactory.getCost($stateParams.costId).then(function (response) {
        $scope.cost = response
        $scope.cost.date = new Date($scope.cost.date)
      })
    } else {
      $scope.cost = {
        when: new Date()
      }
    }

    $scope.isSubmitDisabled = function () {
      return $scope.cost.description === undefined || $scope.cost.price === undefined
    }

    $scope.save = function () {
      costFactory.save($scope.cost).then(function (response) {
        resetForm()
      })
    }

    $scope.getCost = function (costId) {
      costFactory.getCost(costId).then(function (cost) {

      })
    }

    $scope.update = function () {
      costFactory.update($scope.cost).then(function (response) {
        if (response.status === 200) {
          $state.go('cost_list')
        }
      })
    }

    $scope.list = function () {
      costFactory.getAll().then(function (response) {
        $scope.costs = response
      })
    }

    $scope.removeCost = function (costId) {
      var myPopup = $ionicPopup.confirm({
        template: 'Usunąć koszt?',
        title: 'Wycofanie kosztu'
      })
      myPopup.then(function (res) {
        if (res) {
          costFactory.delete(costId).then(function (response) {
            $state.go('cost_list')
          })
        }
      })
    }

    function resetForm () {
      $scope.cost.description = null
      $scope.cost.price = null
      $scope.cost.when = new Date()
    }
  }

  angular.module('goatfarm.controllers').controller('CostCtrl', CostCtrl)
}())
