/**
 * Created by sokoloj1 on 2017-01-31.
 */

/**
 * Created by sokoloj1 on 2017-01-31.
 */

(function () {
  function CostsCtrl ($scope, $state, costFactory) {
    $scope.costs = {}
    $scope.filter = {}
    $scope.filter.when = null

    function init () {
      $scope.pageSize = 5
      fetchCosts([])
    }

    init()

    $scope.loadMore = function () {
      $scope.pageSize += 5
      fetchCosts([])
    }

    $scope.clearFilter = function () {
      $scope.filter.when = null
      $scope.pageSize = 5
      fetchCosts([])
    }

    function fetchCosts (filter) {
      costFactory.getCosts($scope.pageSize, filter).then(function (response) {
        $scope.costs = response.data
        jQuery.each($scope.costs, function () {
          this.when = new Date(this.when) // transform string to date
        })
      })
    };

    $scope.filterMilkStats = function () {
      var filter = []
      if ($scope.filter.when) {
        filter.push({'fieldName': 'when', 'operator': 'equals', 'value': $scope.filter.when})
      }
      $scope.pageSize = 5
      fetchCosts(filter)
    }

    $scope.goToEdit = function (costId) {
      $state.go('cost_edit', {'costId': costId})
    }
  }

  angular.module('goatfarm.controllers').controller('CostsCtrl', CostsCtrl)
}())
