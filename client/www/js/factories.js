(function () {
  var clientsFactory = function ($http, serverHost) {
    return {
      getClients: function () {
        return $http({
          method: 'GET',
          url: serverHost + '/clients',
          params: {
            sort: [{ 'fieldName': 'name', 'order': 'asc' }]
          }
        }).then(function (response) {
          return response.data
        })
      }
    }
  }

  var productsFactory = function ($http, serverHost) {
    return {
      getProducts: function () {
        return $http({
          method: 'GET',
          url: serverHost + '/products',
          params: {
            sort: [{ 'fieldName': 'order', 'order': 'asc' }]
          }
        }).then(function (response) {
          return response.data
        })
      }
    }
  }

  var salesFactory = function ($http, serverHost) {
    return {
      getSales: function (pageSize, filter) {
        return $http({
          method: 'GET',
          url: serverHost + '/sales',
          params: {
            limit: pageSize
          }
        })
      },
      getSale: function (saleId) {
        return $http.get(serverHost + '/sales/sale/' + saleId).then(function (response) {
          return response.data
        })
      },
      save: function (sale) {
        return $http({
          method: 'post',
          url: serverHost + '/sales',
          data: sale
        })
      },
      update: function (sale) {
        return $http({
          method: 'put',
          url: serverHost + '/sales/sale',
          data: sale
        })
      },
      delete: function (saleId) {
        return $http({
          method: 'delete',
          url: serverHost + '/1/objects/sales/' + saleId
        })
      }
    }
  }

  var costFactory = function ($http, serverHost) {
    return {
      getCosts: function (pageSize, filter) {
        return $http({
          method: 'GET',
          url: serverHost + '/costs',
          params: {
            limit: pageSize
          }
        })
      },
      getCost: function (costId) {
        return $http.get(serverHost + '/costs/cost/' + costId).then(function (response) {
          return response.data
        })
      },
      save: function (cost) {
        return $http({
          method: 'post',
          url: serverHost + '/costs',
          data: cost
        })
      },
      update: function (cost) {
        return $http({
          method: 'put',
          url: serverHost + '/costs/cost',
          data: cost
        })
      },
      delete: function (costId) {
        return $http({
          method: 'delete',
          url: serverHost + '/1/objects/sales/' + costId
        })
      }
    }
  }

  var milkStatFactory = function ($http, serverHost) {
    return {
      save: function (milk) {
        return $http({
          method: 'POST',
          url: serverHost + '/1/objects/milk_stats',
          data: milk
        })
      },
      getAll: function (pageSize, filter) {
        return $http({
          method: 'GET',
          url: serverHost + '/1/objects/milk_stats',
          params: {
            pageSize: pageSize,
            pageNumber: 1,
            sort: [{ 'fieldName': 'id', 'order': 'desc' }],
            deep: true,
            filter: JSON.stringify(filter)
          }
        })
      },
      update: function (milk) {
        return $http({
          method: 'PUT',
          url: serverHost + '/1/objects/milk_stats/' + milk.id,
          data: milk
        })
      },
      get: function (milkId) {
        return $http.get(serverHost + '/1/objects/milk_stats/' + milkId).then(function (response) {
          return response.data
        })
      },
      delete: function (milkId) {
        return $http({
          method: 'delete',
          url: serverHost + '/1/objects/milk_stats/' + milkId
        })
      }
    }
  }

  var balanceFactory = function ($http, serverHost) {
    return {
      getIncome: function () {
        return $http({
          method: 'GET',
          url: serverHost + '/1/query/data/incom'
        }).then(function (response) {
          return response.data
        })
      },
      getOutcome: function (serverHost) {
        return $http({
          method: 'GET',
          url: serverHost + '/1/query/data/outcome'
        }).then(function (response) {
          return response.data
        })
      },
      getIncomeInDateRange: function (from, to, serverHost) {
        return $http({
          method: 'GET',
          url: serverHost + '/1/query/data/incomInDateRange',
          params: {
            parameters: {
              'from': from,
              'to': to
            }
          }
        }).then(function (response) {
          return response.data
        })
      }
    }
  }

  angular.module('goatfarm.factories', [])
    .factory('clientsFactory', clientsFactory)
    .factory('productsFactory', productsFactory)
    .factory('salesFactory', salesFactory)
    .factory('costFactory', costFactory)
    .factory('milkStatFactory', milkStatFactory)
    .factory('balanceFactory', balanceFactory)
}())
