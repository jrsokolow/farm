(function() {
    
    var filterSalesByDate = function() {
        return function(sales, from) {
            var result = [];
            if(sales && from) {
                for (var i=0; i<sales.length; i++) {
                    if (sales[i].date >= from)  {
                        result.push(sales[i]);
                    }
                }
            }
            if(result.length === 0 && !from) {
                return sales;
            } else {
                return result;
            }                 
        };   
    };

    angular.module('goatfarm.filters', [])
        .filter('filterSalesByDate', filterSalesByDate);
        
}());