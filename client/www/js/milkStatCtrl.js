/**
 * Created by sokoloj1 on 2017-01-31.
 */

(function() {

  function MilkStatCtrl($scope, $stateParams, $state, milkStatFactory, $ionicPopup) {

    if($stateParams.id) {
      milkStatFactory.get($stateParams.id).then(function(response) {
        $scope.milkStat = response;
        $scope.milkStat.when = new Date($scope.milkStat.when);
      });
    } else {
      $scope.milkStat = {};
      $scope.milkStat.when = new Date();
    }

    $scope.isSubmitDisabled = function() {
      return $scope.milkStat.goats === undefined || $scope.milkStat.milk === undefined || $scope.milkStat.when === undefined;
    }

    $scope.save = function() {
      milkStatFactory.save($scope.milkStat).then(function (response) {
        resetForm();
      });
    }

    $scope.get = function(milkStatId) {
      milkStatFactory.get(milkStatId).then(function(milkStat) {

      });
    }

    $scope.update = function() {
      milkStatFactory.update($scope.milkStat).then(function(response) {
        if(response.status === 200) {
          $state.go("milk_stat_list");
        }
      });
    }

    $scope.list = function() {
      milkStatFactory.getAll().then(function (response) {
          $scope.milkStats = response.data.data;
      });
    };

    $scope.remove = function(id) {
      var myPopup = $ionicPopup.confirm({
        template:'Usunąć koszt?',
        title:'Wycofanie kosztu'
      });
      myPopup.then(function(res) {
        if(res) {
          milkStatFactory.delete(id).then(function(response) {
            $state.go("milk_stat_list");
          });
        }
      });
    };

    function resetForm() {
      $scope.milkStat = {};
      $scope.milkStat.when = new Date();
    }

  }

  angular.module('goatfarm.controllers').controller('MilkStatCtrl', MilkStatCtrl);

}());
