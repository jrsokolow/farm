/**
 * Created by sokoloj1 on 2017-01-31.
 */

/**
 * Created by sokoloj1 on 2017-01-31.
 */

(function() {

  function MilkStatsCtrl($scope, $state, milkStatFactory) {

    $scope.milkStats = {};
    $scope.filter = {};
    $scope.filter.when = null;

    function init() {
      $scope.pageSize = 5;
      fetchMilkStats([]);
    }

    init();

    $scope.loadMore = function() {
      $scope.pageSize += 5;
      fetchMilkStats([]);
    }

    $scope.clearFilter = function() {
      $scope.filter.when = null;
      $scope.pageSize = 5;
      fetchMilkStats([]);
    }

    function fetchMilkStats(filter) {
      milkStatFactory.getAll($scope.pageSize, filter).then(function(response) {
        $scope.milkStats = response.data.data;
        jQuery.each($scope.milkStats, function() {
          this.when = new Date(this.when); // transform string to date
        });
      });
    };

    $scope.filterMilkStats = function() {
      var filter = [];
      if($scope.filter.when) {
        filter.push({"fieldName":"when", "operator":"equals", "value":$scope.filter.when});
      }
      $scope.pageSize = 5;
      fetchMilkStats(filter);
    }

    $scope.edit = function(id) {
      $state.go("milk_stat_edit",{'id':id});
    }

  }

  angular.module('goatfarm.controllers').controller('MilkStatsCtrl', MilkStatsCtrl);

}());

