(function () {
  function SaleCtrl ($scope, $stateParams, $state, $filter, clientsFactory, productsFactory, salesFactory, $ionicPopup) {
    productsFactory.getProducts().then(function (products) {
      $scope.products = products
    }).then(function () {
      clientsFactory.getClients().then(function (clients) {
        $scope.clients = clients
      }).then(function () {
        if ($stateParams.saleId) {
          salesFactory.getSale($stateParams.saleId).then(function (sale) {
            $scope.sale = sale
            $scope.sale.date = new Date($scope.sale.date)
            $scope.sale.product = $filter('filter')($scope.products, { id: sale.productId })[0]
            if (sale.clientId) {
              $scope.sale.client = $filter('filter')($scope.clients, { id: sale.clientId })[0]
            }
          })
        } else {
          $scope.sale = {}
          $scope.sale.date = new Date()
          $scope.sale.paid = true
          $scope.sale.product = $scope.products[0]
          $scope.sale.client = $filter('filter')($scope.clients, { name: 'Inny' })[0]
        }
      })
    })

    $scope.save = function (sale) {
      sale.productId = sale.product.id
      if (sale.client) {
        sale.clientId = sale.client.id
      }
      salesFactory.save(sale).then(function (response) {
        if (response.status === 200) {
          resetSale(sale)
        }
      })
    }

    $scope.update = function (sale) {
      sale.productId = sale.product.id
      sale.clientId = sale.client.id
      console.log(sale.date)
      salesFactory.update(sale).then(function (response) {
        if (response.status === 200) {
          $state.go('sold')
        }
      })
    }

    function resetSale (sale) {
      sale.product = $scope.products[0]
      sale.client = null
      sale.date = new Date()
      sale.price = null
      sale.paid = true
    }

    $scope.isSubmitDisabled = function () {
      if ($scope.sale) {
        return $scope.sale.product === undefined || $scope.sale.date === undefined || $scope.sale.price === undefined
      } else {
        return false
      }
    }

    $scope.removeSale = function (saleId) {
      var myPopup = $ionicPopup.confirm({
        template: 'Usunąć sprzedaż?',
        title: 'Wycofanie sprzedaży'
      })
      myPopup.then(function (res) {
        if (res) {
          salesFactory.delete(saleId).then(function (response) {
            $state.go('sold')
          })
        }
      })
    }
  };

  angular.module('goatfarm.controllers').controller('SaleCtrl', SaleCtrl)
}())
