(function () {
  function SoldCtrl ($scope, productsFactory, clientsFactory, salesFactory, $state, $filter) {
    function init () {
      productsFactory.getProducts().then(function (products) {
        $scope.products = products
      }).then(function () {
        clientsFactory.getClients().then(function (clients) {
          $scope.clients = clients
        }).then(function () {
          $scope.pageSize = 5
          $scope.filter = {}
          $scope.filter.product = null
          $scope.filter.client = null
          $scope.filter.from = null
          fetchSales()
        })
      })
    };

    init()

    $scope.goToEdit = function (saleId) {
      $state.go('sale_edit', { 'saleId': saleId })
    }

    function fetchSales (filter) {
      salesFactory.getSales($scope.pageSize, filter).then(function (sales) {
        $scope.sales = sales.data
        jQuery.each($scope.sales, function () {
          this.date = new Date(this.date) // transform string to date
          if (this.clientId) {
            this.client = $filter('filter')($scope.clients, { id: this.clientId })[0].name
          }
          this.product = $filter('filter')($scope.products, { id: this.productId })[0].name
        })
      })
    };

    $scope.loadMore = function () {
      $scope.pageSize = $scope.pageSize + 5
      fetchSales([])
    }

    $scope.initLoad = function () {
      fetchSales([])
    }

    $scope.filterSales = function () {
      var filter = []
      if ($scope.filter.product) {
        var product = $filter('filter')($scope.products, { name: $scope.filter.product })[0]
        if (product) {
          filter.push({ 'fieldName': 'productId', 'operator': 'in', 'value': product.id })
        }
      }
      if ($scope.filter.client) {
        var client = $filter('filter')($scope.clients, { name: $scope.filter.client })[0]
        if (client) {
          filter.push({ 'fieldName': 'clientId', 'operator': 'in', 'value': client.id })
        }
      }
      if ($scope.filter.from) {
        filter.push({ 'fieldName': 'date', 'operator': 'equals', 'value': $scope.filter.from })
      }

      $scope.pageSize = 5
      fetchSales(filter)
    }

    $scope.clearFilter = function () {
      $scope.filter = {}
      $scope.initLoad([])
    }
  };

  angular.module('goatfarm.controllers').controller('SoldCtrl', SoldCtrl)
}())
